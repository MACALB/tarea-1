import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-x-objetos',
  templateUrl: './x-objetos.component.html',
  styleUrls: ['./x-objetos.component.css']
})
export class XObjetosComponent implements OnInit {

  @Input() x: string;

  constructor() { }

  ngOnInit() {
  }

}
