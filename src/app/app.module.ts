import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormularioComponent } from './formulario/formulario.component';
import { ListadoObjetosComponent } from './listado-objetos/listado-objetos.component';
import { XObjetosComponent } from './x-objetos/x-objetos.component';

@NgModule({
  declarations: [
    AppComponent,
    FormularioComponent,
    ListadoObjetosComponent,
    XObjetosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
