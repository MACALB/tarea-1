import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadoObjetosComponent } from './listado-objetos.component';

describe('ListadoObjetosComponent', () => {
  let component: ListadoObjetosComponent;
  let fixture: ComponentFixture<ListadoObjetosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListadoObjetosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadoObjetosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
