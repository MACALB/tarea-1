import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  /*
  Declaro 6 variables:

  titulo: creo una variable de tipo string, que utilizo en el constructor y le asigno un titulo.

  descripcion: creo una variable de tipo string, que utilizo en el constructor y le asigno una descripcion.

  date: se crea una variable de tipo Date, que contine un objeto Date y que representa una funcionalidad de fecha y hora en TypeScript.
  Nos permite obtener o configurar el año, mes y día, hora, minuto, segundo y milisegundo.

  year: se crea una variable de tipo number, que va ha guardar el valor que se obtenga del metodo getFullYear(), que este caso es el año

  day: se crea una variable de tipo number, que va ha guardar el valor que se obtenga del metodo getDate(), que este caso es el día

  month: se crea una variable de tipo number, que va ha guardar el valor que se obtenga del metodo getMonth(), que este caso es el mes
  */

   titulo: string;
   descripcion: string;
   date: Date = new Date();
   year: number =  this.date.getFullYear(); // Obtenga el año como un número de cuatro dígitos (yyyy)
   day: number = this.date.getDate(); // Obtenga el día como un número (1-31)
   /*
   El método getMonth () devuelve el mes de una fecha como un número del 0 al 11.
   Para obtener el mes correcto, debo agregar o sumar 1
   */
   month: number = this.date.getMonth() + 1;

  constructor() {
    // Esta variable se utiliza el en formulario.component.html {{ }}
    this.titulo = 'SPA - Single Page Application';
    // Esta variable se utiliza el en formulario.component.html {{ }}
    this.descripcion = 'Esta es una SPA, a continuación utilizaremos interpolación de doble corchete.';
}

  ngOnInit() {
  }

}
